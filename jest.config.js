const baseConfig = require('./jest.config.base');

module.exports = {
  ...baseConfig,
  projects: ['<rootDir>/packages/*/jest.config.js'],
  coverageDirectory: '<rootDir>/coverage/',
  collectCoverageFrom: [
    '<rootDir>/packages/*/src/**/*.{ts,tsx}',
    '!<rootDir>/packages/*/src/generated.{ts,tsx}',
    '!<rootDir>/packages/*/src/**/types/*',
    '!<rootDir>/packages/*/src/tests/*',
    '!<rootDir>/packages/*/src/**/*.d.ts',
    '!<rootDir>/packages/*/src/**/*.stories.*',
    '!<rootDir>/packages/*/src/**/*.types.*',
  ],

  coverageReporters: ['json-summary', 'text-summary', 'lcov', 'text', 'clover'],
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: 80,
    },
  },
  testURL: 'http://localhost/',
  moduleNameMapper: {
    '.json$': 'identity-obj-proxy',
  },
  moduleDirectories: ['node_modules'],
  snapshotSerializers: [],
};
