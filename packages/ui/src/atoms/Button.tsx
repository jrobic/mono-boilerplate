import React from 'react';

export interface ButtonProps {
  label: string;
  onClick?: () => void;
}

/**
 * Primary UI component for user interaction
 */
export const Button: React.FC<ButtonProps> = ({ label, ...props }) => {
  return (
    <button type="button" {...props}>
      {label}
    </button>
  );
};
