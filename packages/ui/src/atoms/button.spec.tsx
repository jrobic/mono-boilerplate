import React from 'react';
import { render } from '@testing-library/react';
import { Button } from './Button';

describe('When use Button', () => {
  it('should render snapshot', () => {
    const { asFragment } = render(<Button label="awesome" />);
    expect(asFragment()).toMatchSnapshot();
  });
});
