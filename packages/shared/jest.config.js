const baseConfig = require('../../jest.config.base');

const packageName = require('./package.json').name.split('@acme/').pop();

module.exports = {
  ...baseConfig,
  roots: [`<rootDir>/packages/${packageName}`],
  collectCoverageFrom: ['src/**/*.{ts,tsx}'],
  setupFiles: [],
  setupFilesAfterEnv: [`<rootDir>/packages/${packageName}/src/setupJest.ts`],
  testRegex: `(packages/${packageName}/.*/src/.*|\\.(test|spec))\\.tsx?$`,
  testURL: 'http://localhost/',

  moduleDirectories: ['node_modules'],
  modulePaths: [`<rootDir>/packages/${packageName}/src/`],
  snapshotSerializers: [],
  name: packageName,
  displayName: packageName,
  rootDir: '../..',
};
