import { sayHello } from '.';

test('should return string with default name', () => {
  expect(sayHello()).toEqual('Hello, World');
});
