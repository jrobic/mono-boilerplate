const baseConfig = require('../../jest.config.base');

const packageName = require('./package.json').name.split('@acme/').pop();

module.exports = {
  ...baseConfig,
  verbose: true,
  testEnvironment: 'jest-environment-jsdom-fourteen',
  roots: [`<rootDir>/packages/${packageName}`],
  setupFiles: ['react-app-polyfill/jsdom'],
  setupFilesAfterEnv: [`<rootDir>/packages/${packageName}/src/setupTests.ts`],
  testRegex: `(<rootDir>/packages/${packageName}/.*/src/.*|\\.(test|spec))\\.tsx?$`,
  testURL: 'http://localhost/',
  transform: {
    '^.+\\.(js|jsx|ts|tsx)$': `react-scripts/config/jest/babelTransform.js`,
    '^.+\\.css$': `react-scripts/config/jest/cssTransform.js`,
    '^(?!.*\\.(js|jsx|ts|tsx|css|json)$)': `react-scripts/config/jest/fileTransform.js`,
  },
  moduleDirectories: ['node_modules'],
  modulePaths: [`<rootDir>/packages/${packageName}/src/`],
  moduleNameMapper: {
    '^react-native$': 'react-native-web',
    '^.+\\.module\\.(css|sass|scss)$': 'identity-obj-proxy',
  },
  moduleFileExtensions: [
    'web.js',
    'js',
    'web.ts',
    'ts',
    'web.tsx',
    'tsx',
    'json',
    'web.jsx',
    'jsx',
    'node',
  ],
  watchPlugins: ['jest-watch-typeahead/filename', 'jest-watch-typeahead/testname'],
  snapshotSerializers: [],
  name: packageName,
  displayName: packageName,
  rootDir: '../..',
};
