module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  transformIgnorePatterns: ['[/\\\\]node_modules[/\\\\].+\\.tsx?$'],
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest',
  },
};
